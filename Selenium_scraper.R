#########################
####Rselenium scraper####
#########################

#############
###library###
#############

library(RSelenium)
library(tidyverse)
library(lubridate)
library(rtweet)
library(httpuv)

get_tweets_on_page <- function(page_tweets){
  
  page_tweet_content<-vector(mode = "list", length(page_tweets))
  
  for(i in 1:length(page_tweet_content)){
    curr_tweet<-tryCatch(page_tweets[[i]]$findChildElement(using= "class name", "content"), error = function(e) e)
    tweet_id_obj<-page_tweets[[i]]$getElementAttribute("data-item-id")
    tweet_id<-tweet_id_obj[[1]]
    
    tweet_datetime_obj<-page_tweets[[i]]$findChildElement(using= "class name", "tweet-timestamp")
    tweet_datetime<-tweet_datetime_obj$getElementAttribute("title")
    tweet_datetime<-tweet_datetime[[1]]%>%parse_date_time("%h:%M - d b! Y")

    
    if(class(curr_tweet)[1]=="simpleError"){
      next
    }
    
    RT_user <- "none"
    RT_text <- "none"
    quoted_user <- "none"
    quoted_text <- "none"
    media_link<-"none"
    
    ##check for retweet
    retweet<-tryCatch(page_tweets[[i]]$findChildElement(using= "class name", "content"), error = function(e) e)
    
    if(class(retweet)[1]=="simpleError"){
      
      ###check for quote tweet
      quote_tweet <- tryCatch(page_tweets[[i]]$findChildElement(using= "class name", "QuoteTweet-authorAndText"), 
                              error = function(e) e)
      
      if(class(quote_tweet)[1]=="simpleError"){
        
        #if no quote tweet then look for media
        media_check<-tryCatch(page_tweets[[i]]$findChildElement(using= "class name", "js-media-container"), error = function(e) e)
        
        if(class(media_check)[1]=="simpleError"){

        }else{
          media_obj <-page_tweets[[i]]$findChildElement(using= "class name", "js-macaw-cards-iframe-container")
          media_link<-media_obj$getElementAttribute("data-card-url")
          media_link<-media_link[[1]]
        }
        
      }else{
        quoted_user_obj<-quote_tweet$findChildElement(using = "class name", "username")
        quoted_user_l<-quoted_user_obj$getElementAttribute("innerHTML")%>%str_split(">")%>%lapply(function(x) return(x[2]))
        quoted_user<-quoted_user_l[[1]]%>%str_split("<")%>%lapply(function(x) return(x[1]))
        quoted_user<-quoted_user[[1]]
        
        quoted_text_obj<-quote_tweet$findChildElement(using = "class name", "QuoteTweet-text")
        quoted_text_obj<-quoted_text_obj$getElementAttribute("innerHTML")
        quoted_text<- quoted_text_obj[[1]]
      }
    }else{
      RT_user_obj<-retweet$findChildElement(using = "class name", "username")
      RT_user_obj<-RT_user_obj$getElementAttribute("innerHTML")%>%unlist%>%str_split(">")
      RT_user_obj<-RT_user_obj[[1]][2]%>%str_split("<")
      RT_user<-RT_user_obj[[1]][1]
      
      RT_text_obj<-retweet$findChildElement(using = "class name", "js-tweet-text")
      RT_text<-RT_text_obj$getElementAttribute("innerHTML")%>%unlist
    }

    tweet_text_obj<-page_tweets[[i]]$findChildElement(using= "css selector", "p")
    tweet_text <- tweet_text_obj$getElementAttribute("innerHTML")
    tweet_text <- tweet_text[[1]]
    
    tweet_footer<-page_tweets[[i]]$findChildElement(using= "class name", "content")
    
    #comments
    tweet_reply_obj<-tweet_footer$findChildElement(using = "class name", "js-actionReply")
    tweet_footer_actions<-tweet_reply_obj$findChildElement(using = "class name", "ProfileTweet-actionCountForPresentation")
    num_comments<-tweet_footer_actions$getElementAttribute("innerHTML")
    num_comments<-num_comments[[1]]
    
    #RTs
    tweet_RT_obj<-tweet_footer$findChildElement(using = "class name", "js-toggleRt")
    tweet_footer_actions<-tweet_RT_obj$findChildElement(using = "class name", "ProfileTweet-actionCountForPresentation")
    num_RTs<-tweet_footer_actions$getElementAttribute("innerHTML")
    num_RTs<-num_RTs[[1]]
    
    #faves
    tweet_fav_obj<-tweet_footer$findChildElement(using = "class name", "js-actionFavorite")
    tweet_footer_actions<-tweet_fav_obj$findChildElement(using = "class name", "ProfileTweet-actionCountForPresentation")
    num_faves<-tweet_footer_actions$getElementAttribute("innerHTML")
    num_faves<-num_faves[[1]]
    
    tweet_info<-page_tweets[[i]]$findChildElement(using= "class name", "js-actionable-tweet") 
    tweeter<-tweet_info$getElementAttribute("data-screen-name")
    tweeter<-tweeter[[1]]
    
    curr_tweet_list <- list(tweet_id= tweet_id, date_time = tweet_datetime, text = tweet_text, link = media_link, 
                            num_com=num_comments, num_RT = num_RTs,num_fav = num_faves, tweeter=tweeter, 
                            quoted_user = quoted_user, quoted_text= quoted_text, RT_user=RT_user, RT_text=RT_text)
    
    page_tweet_content[[i]]<-curr_tweet_list
    
    print(str_c("finished: ", i,"/", length(page_tweet_content)))
  }
  
  return(page_tweet_content)
}

get_users_tweets <- function(){

  pages_tweets <- vector(mode="list", length=10000)
  more_items=TRUE
  
  while(more_items){
    #paginate
    remDr_cli$executeScript("window.scrollTo(0,document.body.scrollHeight);")
    new_height = remDr_cli$executeScript("return document.body.scrollHeight")
    
    more_items_class <- tryCatch(remDr_cli$findElement("class name", "has-more-items"), error = function(e) e)%>%class
    more_items_c<-more_items_class == "webElement"
    more_items<-more_items_c[1]
  }
  
  #get tweets
  page_stream<-remDr_cli$findElement(using = 'id', "stream-items-id")
  page_tweets<-page_stream$findChildElements(using= "class name", "stream-item")
  
  pages_tweets_all<-get_tweets_on_page(page_tweets)
  
  to_rm<-pages_tweets_all%>%lapply(length)%>%{which(.==0)}
  if(length(to_rm)>0){
    full_pages_tweets<-pages_tweets_all[-to_rm]
  }else{
    full_pages_tweets<-pages_tweets_all
  }
  
  D_tweets<-full_pages_tweets%>%{do.call("rbind", .)}%>%data.frame
  
  return(D_tweets)
}

scroll_to_tweet<-function(id, tweet_date){
  
  search_query<-"from:staceyabrams since:START_DATE until:END_DATE"
  
  search_bar<-remDr_cli$findElement(using= "class name", "search-input") 
  usrElem$sendKeysToElement(list("CaseNthieme"))
  
  pageElem <- remDr_cli$findElement("css", "body")
  pageElem$sendKeysToElement(list(key = "end"))
  remDr_cli$executeScript("return document.body.scrollHeight")
  
  page_stream<-remDr_cli$findElement(using = 'id', "stream-items-id")
  page_tweets<-page_stream$findChildElements(using= "class name", "stream-item")
  
  latest_tweet<-page_tweets%>%tail(1)
  latest_id<-latest_tweet[[1]]$getElementAttribute("data-item-id")
  latest_id<-latest_id[[1]]%>%as.numeric
  
  passed_tweet<-latest_id>id
  
  while(passed_tweet){
    pageElem$sendKeysToElement(list(key = "end"))

    page_stream<-remDr_cli$findElement(using = 'id', "stream-items-id")
    page_tweets<-page_stream$findChildElements(using= "class name", "stream-item")
    
    latest_tweet<-page_tweets%>%tail(1)
    latest_id<-latest_tweet[[1]]$getElementAttribute("data-item-id")
    latest_id<-latest_id[[1]]%>%as.numeric
    
    passed_tweet<-latest_id>id
  }
  
  
}

#################
###Twitter API###
#################
Twitter_handles<-read.csv("GA_twitter_handles.csv")
handles<-Twitter_handles$twitter_handle%>%as.character()%>%str_sub(2, nchar(.))%>%str_remove_all(" ")

username <- handles[2]

D_tweets<-get_timeline(username, n = 3200)%>%mutate(
  tweeter = case_when(is_quote==TRUE~quoted_screen_name, 
                      is_retweet==TRUE~retweet_screen_name, 
                      (is_quote==FALSE)&(is_retweet==FALSE)~screen_name
  ),
  num_com=rep(NA, n()),
   date_time= created_at%>%parse_date_time("ymdHMS")
)%>%select(tweet_id=status_id, date_time= created_at, text, link=urls_url,num_com,num_fav=favorite_count, 
           num_RT=retweet_count, tweeter, quoted_user = quoted_user_id, quoted_text = quoted_text, RT_user = retweet_user_id,
           RT_text = retweet_text)

if(nrow(D_tweets)>3000){
  
  last_date<-D_tweets%>%arrange(date_time)%>%head(1)%>%select(date_time)
  last_date<-last_date$date_time%>%str_sub(1,10)%>%ymd
  year_before_last_date<-last_date-365
  
  remaining_tweets=TRUE
  
  remDr <- rsDriver(browser ="firefox", port= 4134L)
  remDr_cli<-remDr$client
  
  search_query<-str_c("from:", username, " ","since:", year_before_last_date, " until:",last_date)
  
  sorted_search_page<-str_c("https://twitter.com/search?f=tweets&vertical=default&q=from%3A",username, "%20since%3A",year_before_last_date,
                            "%20until%3A", last_date, "&src=typd")
  remDr_cli$navigate(sorted_search_page)
  
  while(remaining_tweets){
    ############
    ###Scrape###
    ############
    
    usersTweets<-get_users_tweets()
    
    D_tweets<- rbind(as.matrix(usersTweets), as.matrix(D_tweets))
    
    ##check for more tweets in another epoch
  
    last_date<-year_before_last_date
    year_before_last_date<-year_before_last_date-365
    
    search_query<-str_c("from:", username, " ","since:", year_before_last_date, " until:",last_date)
    
    sorted_search_page<-str_c("https://twitter.com/search?f=tweets&vertical=default&q=from%3A",username, "%20since%3A",year_before_last_date,
                              "%20until%3A", last_date, "&src=typd")
    remDr_cli$navigate(sorted_search_page)
    
    page_stream<-remDr_cli$findElement(using = 'id', "stream-items-id")
    page_tweets<-page_stream$findChildElements(using= "class name", "stream-item")
    
    remaining_tweets <- length(page_tweets)>0
  }
}

D_tweets_complete<-rbind(as.matrix(D_tweets_api), D_tweets)

assign(str_c("D_tweets", "_",username), D_tweets_complete)

fwrite(get(str_c("D_tweets", "_",username)), str_c("D_tweets", "_",username, ".csv"))


###############
###Graveyard###
###############

###########
###Login###
###########

#don't actually need to log in 

site <- "https://twitter.com/login" 
remDr_cli$navigate(site)

Sys.sleep(5)

usrElem<-remDr_cli$findElement(using = 'class name',"js-username-field")
usrElem$sendKeysToElement(list("CaseNthieme"))

pwdElem<-remDr_cli$findElement(using = 'class name',"js-password-field")
pwdElem$sendKeysToElement(list("p54jag36pfehw9sgcm"))

loginElem <- remDr_cli$findElement(using = 'css selector',"button")
loginElem$clickElement()


